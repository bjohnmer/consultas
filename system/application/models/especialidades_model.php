<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Especialidades_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall($num,$offset)
	{
		$consulta = $this->db->get('especialidades', $num, $offset);
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function find($data, $num, $offset)
	{
		/*$sql = "SELECT medicos.*, personas.*, especialidades.* FROM medicos JOIN personas ON medicos.persona_id = personas.id_persona JOIN especialidades ON medicos.especialidad_id = especialidades.id_especialidad WHERE personas.nombres_persona LIKE ?";
		
		$consulta = $this->db->query($sql, '%'.mysql_real_escape_string($data['texto']).'%');
		*/
		$this->db->like('nombre_especialidad', $data['texto']);
		$consulta = $this->db->get('especialidades', $num, $offset);
		
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		
		$consulta->free_result();
		return $data;
	}


	public function getallnp()
	{
		$consulta = $this->db->get('especialidades');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getreg($id)
	{
		$consulta = $this->db->where("id_especialidad", $id)->get('especialidades');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getTotalCount()
	{
		$consulta = $this->db->get('especialidades');
		if ($consulta->num_rows()) {
			$data = $consulta->num_rows();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function create($data)
	{
		$data_especialidad = array (
							'nombre_especialidad'	=> $data['nombre_especialidad']
						);
		$esp = $this->db->insert('especialidades', $data_especialidad);
		
		if ($esp) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update($data)
	{
		$data_especialidad = array (
							'nombre_especialidad'	=> $data['datos']['nombre_especialidad']
						);
		
		$esp = $this->db->where('id_especialidad', $data['datos']['id_especialidad'])->update('especialidades', $data_especialidad);
		
		if ($esp) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function destroy($data)
	{
		$per = $this->db->where('id_especialidad', $data['id'])->delete('especialidades');
		if ($per) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}