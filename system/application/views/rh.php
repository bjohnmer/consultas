<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo"><h1>Reseña Histórica</h1></div>
    <div id="columcont">
      <p>
        El hospital comenzó a prestar servicio de hospitalización en ares anexa en el hospital Dr. José Gregorio Hernández en el primer piso, en Abril de 1967; donde se trataban los pacientes asegurados. El 05 de septiembre de 1976 se inicio el servicio de consulta externa y triaje; así como el servicio de Laboratorio, Farmacia, Trabajo Social y Archivo en la Av. Independencia donde funciona actualmente la Farmacia Razetti y el Grupo Medico la Paz, hasta el 1 de Marzo de 1979, que fue inaugurado el hospital quedando dividido los servicios de hospitalización, Archivo, RX y Sala de Traumatología en el edificio actual y en el Edificio Camambú frente al hospital funcionaba Laboratorio, Odontología y áreas Administrativas, incluyendo la dirección así como todas las consultas externas. En el año 2002 fue necesario trasladar la consulta externa, archivo, odontología para un modulo de FUNDASALUD en la Urb. La Vega. Esto debido a que fue declarado en Emergencia el edificio Yacambú por derrumbe del cerro posterior al edificio que amenazó con una piedra gigante que penetro hasta uno de los consultorios. Desde esa fecha deferentes sectores vienen solicitando la construcción de una Nueva sede para este hospital.
    </p>
        <p>
        En cuanto al personal directivo. El primer director fue el médico Pediatra Dr. Gilberto Gómez, luego por orden cronológico fueron: Antonio Nicolás Briceño, Francisco Carreño Medico Especialista en Cirugía General; luego el Patólogo Dr. Anastasio Sáez, el anestesiólogo Dr. Cruz Araujo, el Otorrinolaringólogo, Dr. Bladimir Márquez; Luego el Anestesiólogo Dr. Jesús María Niño y la Medico Radiólogo Dra. Italia de Altuve. Dr. José J. Carmona, luego el Dr. Indelbrando Suarez Chourio Medico Ginecólogo y actualmente mi persona, medico Neurólogo Pediatra. Como secretarias de larga trayectoria la Sra. Mercedes Bastidas de Bencomo y la Sra. Miriam Aldana.
        </p>
    </div>
    <div id="columimg">
      <img src="/img/hospital.jpg">
    </div>
    
  </div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
