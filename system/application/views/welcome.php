<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo"><h1>Sistema de Información Web para el Control de Citas del I.V.S.S. Trujillo</h1></div>
    <div id="logo"><img src="/img/3entrega.fw.png"></div>
    <?=form_open("login");?>
      <div id="contform">
        <h3>Usuarios Autorizados</h3>
        <?php
          echo form_label("Usuario", "usuario");
          $data = array("name"=>"usuario","id"=>"usuario","placeholder"=>"Ingrese su nombre de usuario");
          echo form_input($data);
          echo form_label("Clave", "password");
          $data = array("name"=>"password","id"=>"password","placeholder"=>"Ingrese su nombre de clave");
          echo form_password($data);          
          $data = array("value"=>"Entrar");
          echo form_submit($data);
          echo "<div class='error'>";
          if (validation_errors()) echo validation_errors();
          if ($this->session->flashdata("loged_in_fail")) echo "<p>ERROR:<br>Usted a proporcionado datos inválidos<br>Intente nuevamente</p>";
          echo "</div>";
        ?>
      </div>
      <div id="imgform">
        <img src="/img/key.png">
      </div>
    <?=form_close();?>
  </div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
