<?php

$lang['required']			= "El campo %s es requerido.";
$lang['isset']				= "El campo %s debe contener un valor.";
$lang['valid_email']		= "El campo %s debe contener una dirección de email válida.";
$lang['valid_emails']		= "El campo %s debe contener todas las direcciones de email válidas.";
$lang['valid_url']			= "El campo %s debe contener una URL válida.";
$lang['valid_ip']			= "El campo %s debe contener una IP válida.";
$lang['min_length']			= "El campo %s debe tener al menos %s caracteres.";
$lang['max_length']			= "El campo %s no puede exeder los %s caracteres.";
$lang['exact_length']		= "El campo %s debe tener exáctamente %s caracteres.";
$lang['alpha']				= "El campo %s debe contener sólo caracteres alfabéticos.";
$lang['alpha_numeric']		= "El campo %s debe contener sólo caracteres alfanuméricos.";
$lang['alpha_dash']			= "El campo %s debe contener sólo caracteres alfanuméricos, underscores y giones.";
$lang['alpha_space']		= "El campo %s debe contener sólo caracteres alfabéticos y/o espacios.";
$lang['numeric']			= "El campo %s debe contener sólo números.";
$lang['is_numeric']			= "El campo %s debe contener sólo caracteres numéricos.";
$lang['integer']			= "El campo %s debe contener un entero.";
$lang['regex_match']		= "El campo %s no coincide con el formato especificado.";
$lang['matches']			= "El campo %s no coincide con %s .";
$lang['is_unique'] 			= "El campo %s debe contener un valor único.";
$lang['is_natural']			= "El campo %s debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "El campo %s debe contener un valor mayor a cero.";
$lang['decimal']			= "El campo %s debe contener un número decimal.";
$lang['less_than']			= "El campo %s debe contener un número menor que %s.";
$lang['greater_than']		= "El campo %s debe contener un número mayor que %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */