<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('Form_validation');
		$this->load->model("login_model");
	}
	public function index()
	{
		$this->form_validation->set_rules("usuario", "Usuario", "required|trim|max_length[50]|xss_clean");
		$this->form_validation->set_rules("password", "Clave", "required|trim|max_length[50]|xss_clean");

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('welcome');
		} else {
			$data = $this->login_model->check_login(set_value("usuario"), set_value("password"));
			if ($data){
				$this->session->set_userdata("logged_in"	, TRUE);
				$this->session->set_userdata("user_nombre"	, $data->nombre);
				$this->session->set_userdata("user_apellido", $data->apellido);
				$this->session->set_userdata("user_cargo"	, $data->cargo);
				$this->session->set_userdata("user_nivel"	, $data->nivel);
				$this->session->set_userdata("user_usuario"	, $data->usuario);
				redirect('admin/');
			} else {
				$this->session->set_flashdata("loged_in_fail", TRUE);
				redirect('/');
			}
		}
	}
	public function logout(){
		$this->session->set_userdata("logged_in", FALSE);
		$this->session->sess_destroy();
		redirect('/');
	}
}