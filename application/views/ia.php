<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo">
    <h1>Información Adicional</h1></div>
      <div id="columcont">
        <p><strong>Descripción de la Institución</strong></p>
        <p>La unidad de historias médicas ubicada en los centros asistenciales adscrito al Instituto Venezolano de los Seguros Sociales (I.V.S.S.) representa una unidad de apoyo a la asistencia médica integral que se brinda a los pacientes, la cual se encarga de coordinar la recolección, archivo y custodia de los datos administrativos, sociales y médicos referente a los movimientos de los pacientes en los centros ambulatorios y hospitales del Instituto.</p>
        <p>Esta unidad está estructurada en áreas funcionales, las cuales reflejan la ubicación del personal y definición de funciones por áreas, así mismo se encuentra distribuida en los diferentes niveles de la siguiente manera:</p>
          <br />
          <p>Nivel superior: Representa la unidad máxima, tiene la responsabilidad de administrar los recursos asignados de manera eficiente y necesaria para la ejecución de las decisiones emanadas del Departamento de Registro y Estadística de Salud.</p>
          <p>Nivel operativo: Representa las áreas que tienen la responsabilidad de desarrollar las actividades dirigidas a apoyar la prestación de servicio y asistencia a los pacientes.</p>
          <p><strong>Misión:</strong></p>
          <p>Es la unidad técnica administrativa encargada de dar apoyo a la asistencia médica integral otorgada al ciudadano o usuario de los centros asistenciales, manteniendo la historia clínica del paciente y generando los registros estadísticos de la productividad hospitalaria y ambulatoria de manera eficiente y oportuna.</p>
          <br />
          <p dir="ltr"><strong>Visión:</strong></p>
          <p>Ser una unidad técnica administrativa comprometida con la Institución, aportando flujo de información de los pacientes atendidos en los centros asistenciales de manera oportuna, ágil y continua para la toma de decisiones permitiendo así mantener la atención medica integral requerida por los usuarios.</p>
      </div>
      <div id="columimg">
        <img src="<?=base_url()?>img/hospital.jpg">
      </div>
  </div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
