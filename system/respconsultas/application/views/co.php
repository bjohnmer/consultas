<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo"><h1>Sistema de Información Web para el Control de Citas del I.V.S.S. Trujillo</h1></div>
  	<div id="titulo"><h3>Contacto</h3></div>
  	<?php
      echo form_open("welcome/send_contact");
      if (validation_errors()) echo "<div class='error'>".validation_errors()."</div>";
      
      if (!empty($mensaje)) echo "<div class='success'><p>".$mensaje."</p></div>";

        echo "<div id='contform'>";
        echo form_label("Nombre Completo", "nombre");
        $data = array(
            "name" => "nombre",
            "id" => "nombre",
            "value" => set_value("nombre"),
            "placeholder" => "Escriba su Nombre Completo"
        );
        echo form_input($data);
        
        echo form_label("Correo Electrónico", "email");
        $data = array(
            "name" => "email",
            "id" => "email",
            "value" => set_value("email"),
            "placeholder" => "Escriba su Correo Electrónico"
        );
        echo form_input($data);

        echo form_label("Comentario", "comentario");
        $data = array(
            "name" => "comentario",
            "id" => "comentario",
            "value" => set_value("comentario"),
            "placeholder" => "Escriba su Comentario"
        );
        echo form_textarea($data);
        $data = array(
            "value" => "Enviar Comentario"
        );
        echo form_submit($data);
        echo "</div>";
  		echo form_close();



  	?>
  </div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
