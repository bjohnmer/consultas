<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Citas</h2>
        <a href="<?=base_url()?>index.php/citas/add_form" class="btn btn-primary"><i class='icon-plus icon-white'></i> Nueva Cita</a>
        <?=$this->pagination->create_links();?>
        <?php if (!empty($mensaje)) echo "<br><br><div class='alert alert-success'><p>".$mensaje."</p></div>";?>
        <?php if($records) : ?>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Historia</th>
              <th>Paciente</th>
              <th>Fecha</th>
              <th>Médico</th>
              <th></th>
            </tr>
          </thead>
          <?php  foreach($records as $row) : ?>
            <tr>
              <td><?=$row->historia_id?></td>
              <td><?=$row->paciente_id?></td>
              <td><?=$row->fecha?></td>
              <td><?php

              $data['med'] = $this->medicos->getreg($row->medico_id);

              // echo $data['med']->nombres_persona;
              echo $data['med'][0]->nombres_persona." ". $data['med'][0]->apellidos_persona . " | " . $data['med'][0]->nombre_especialidad;
              ?></td>
              <td><?=anchor(base_url()."index.php/citas/delete/".$row->id_consulta, "<i class='icon-trash'></i> Eliminar", "class='eliminar'")?></td>
            </tr>
          <?php endforeach; ?>
          </table>
        <?=$this->pagination->create_links();?>
    	 <?php else : ?>	
    	         <h2>No hay registros para mostrar</h2>
    	 <?php endif; ?>
      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
</body>
</html>
