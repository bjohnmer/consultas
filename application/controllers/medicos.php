<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medicos extends CI_Controller {
	function __construct(){
		parent::__construct();
		//$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('pagination');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		} else {
			$this->load->model("especialidades_model","especialidades");
			$this->load->model("medicos_model","medicos");
		}
	}
	function getPageConfig()
	{
		$config['base_url'] = base_url().'index.php/medicos/index';
		$config['total_rows'] = $this->medicos->getTotalCount();
		$config['per_page'] = 20;
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		return $config;
	}
	public function index() {

		$config = $this->getPageConfig();
		
		$this->pagination->initialize($config);
		$data['records'] = $this->medicos->getall($config['per_page'],$this->uri->segment(3));

		$this->load->view('medicos_view', $data);

	}
	public function add_form($data = array()) {

		$data['records'] = $this->especialidades->getallnp();
		$this->load->view('medicos_add_view', $data);			
	}

	public function edit_form($data = array()) 
	{

		$data = array(
				'records' 	=> $this->especialidades->getallnp(),
				'consulta' 	=> $this->medicos->getreg($this->uri->segment(3)),
				);
		$this->load->view('medicos_edit_view', $data);

	}

	public function add() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('cedula_medico', 'Cédula', 'required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
		$this->form_validation->set_rules('nombres_persona', 'Nombres', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('apellidos_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('direccion_persona', 'Dirección', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('nacionalidad_persona', 'Nacionalidad', 'required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('primeras_medico', 'Primeras Cítas por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('controles_medico', 'Controles por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('reposos_medico', 'Reposos por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('MPPSPS_medico', 'Código MPPSPS', 'required|xss_clean');
		$this->form_validation->set_rules('horario_medico', 'Horario de Atención', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->add_form();
		}
		else
		{
			$respuesta = $this->medicos->create($_POST);
			if ($respuesta) {
				$data["mensaje"] = "Los datos se almacenaron correctamente";

				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->medicos->getall($config['per_page'],'');
				$this->load->view('medicos_view', $data);

			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
				$this->add_form($data);
			}
		}

	}

	public function edit() 
	{
		$this->load->library('form_validation');
		

		$this->form_validation->set_rules('cedula_medico', 'Cédula', 'required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
		$this->form_validation->set_rules('nombres_persona', 'Nombres', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('apellidos_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('direccion_persona', 'Dirección', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('nacionalidad_persona', 'Nacionalidad', 'required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('primeras_medico', 'Primeras Cítas por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('controles_medico', 'Controles por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('reposos_medico', 'Reposos por día', 'required|numeric|is_natural|xss_clean');
		$this->form_validation->set_rules('MPPSPS_medico', 'Código MPPSPS', 'required|xss_clean');
		$this->form_validation->set_rules('horario_medico', 'Horario de Atención', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit_form();
		}
		else
		{
			$data['datos'] = $_POST;
			$respuesta = $this->medicos->update($data);
			if ($respuesta) {
				$data["mensaje"] = "Los datos se modificaron correctamente";
				
				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->medicos->getall($config['per_page'],'');
				
				$this->load->view('medicos_view', $data);
			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
				$this->edit_form($data);
			}
		}

	}

	public function delete() 
	{
		
		$data['id'] = $this->uri->segment(3);
		$respuesta = $this->medicos->destroy($data);
		if ($respuesta) {
			$data["mensaje"] = "El registro se ha eliminado";
			
			$config = $this->getPageConfig();

			$this->pagination->initialize($config);
			$data['records'] = $this->medicos->getall($config['per_page'],'');
			
			$this->load->view('medicos_view', $data);
		} else {
			$data['mensaje'] = 'Error: No se ha podido eliminar';
			$this->edit_form($data);
		}
	}


	public function find() 
	{
		$config = $this->getPageConfig();
		$data = $_POST;
		$data['records']= $this->medicos->find($data,$config['per_page'],'');
		if ($data['records']) {
						
			$this->pagination->initialize($config);
			
			$this->load->view('medicos_view', $data);
		} else {
			$data['mensaje'] = "Error en la búsqueda";
			$this->load->view('medicos_view', $data);
		}
	}


}