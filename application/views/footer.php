<footer class="dgrojo">
	<div id="hfooter">Copyright <?php echo date("Y");?>, IVSS Todos los derechos Reservados. Desarrollado por: Daniela Becerra, Laiberth Cedeño y Francisco Durán </div>
</footer>
<script type="text/javascript" src="<?=base_url()?>js/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/default.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-transition.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-alert.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-modal.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-dropdown.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-scrollspy.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-tab.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-popover.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-button.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-collapse.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-carousel.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery-ui-1.8.21.custom.min.js"></script>