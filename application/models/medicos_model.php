<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medicos_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall($num,$offset)
	{
		$consulta = $this->db->join('especialidades', 'especialidades.id_especialidad = medicos.especialidad_id')->join('personas', 'personas.id_persona = medicos.persona_id')->get('medicos', $num, $offset);
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function find($data, $num, $offset)
	{
		$sql = "SELECT medicos.*, personas.*, especialidades.* FROM medicos JOIN personas ON medicos.persona_id = personas.id_persona JOIN especialidades ON medicos.especialidad_id = especialidades.id_especialidad WHERE personas.nombres_persona LIKE ?";
		
		$consulta = $this->db->query($sql, '%'.mysql_real_escape_string($data['texto']).'%');

		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getallnp()
	{
		$consulta = $this->db->join('especialidades', 'especialidades.id_especialidad = medicos.especialidad_id')->join('personas', 'personas.id_persona = medicos.persona_id')->get('medicos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getallnpt()
	{
		
		$date = date("H");
		if ($date < 13) $consulta = $this->db->join('especialidades', 'especialidades.id_especialidad = medicos.especialidad_id')->join('personas', 'personas.id_persona = medicos.persona_id')->where('turno_medico', 'Mañana')->get('medicos');
		else if ($date < 23) $consulta = $this->db->join('especialidades', 'especialidades.id_especialidad = medicos.especialidad_id')->join('personas', 'personas.id_persona = medicos.persona_id')->where('turno_medico', 'Tarde')->get('medicos');

		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}


	public function getreg($id)
	{
		$consulta = $this->db->where("id_medico", $id)->join('especialidades', 'especialidades.id_especialidad = medicos.especialidad_id')->join('personas', 'personas.id_persona = medicos.persona_id')->get('medicos');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getTotalCount()
	{
		$consulta = $this->db->get('medicos');
		if ($consulta->num_rows()) {
			$data = $consulta->num_rows();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function create($data)
	{
		$data_persona = array (
							'nombres_persona'		=> $data['nombres_persona'],
							'apellidos_persona'		=> $data['apellidos_persona'],
							'direccion_persona'		=> $data['direccion_persona'],
							'sexo_persona'			=> $data['sexo_persona'],
							'nacionalidad_persona'	=> $data['nacionalidad_persona']
						);
		//print_r($data_persona);
		$per = $this->db->insert('personas', $data_persona);
		$data_medico = array (
							'persona_id' 		=> $this->db->insert_id(),
							'especialidad_id'	=> $data['especialidad_id'],
							'primeras_medico'	=> $data['primeras_medico'],
							'controles_medico'	=> $data['controles_medico'],
							'reposos_medico'	=> $data['reposos_medico'],
							'cedula_medico'		=> strtoupper($data['cedula_medico']),
							'MPPSPS_medico'		=> strtoupper($data['MPPSPS_medico']),
							'turno_medico'		=> $data['turno_medico'],
							'horario_medico'	=> $data['horario_medico'],
							'condicion_medico'	=> $data['condicion_medico']
						);
		$med = $this->db->insert('medicos', $data_medico);
		if ($per) {
			if ($med) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function update($data)
	{
		$data_persona = array (
							'nombres_persona'		=> $data['datos']['nombres_persona'],
							'apellidos_persona'		=> $data['datos']['apellidos_persona'],
							'direccion_persona'		=> $data['datos']['direccion_persona'],
							'sexo_persona'			=> $data['datos']['sexo_persona'],
							'nacionalidad_persona'	=> $data['datos']['nacionalidad_persona']
						);
		
		$per = $this->db->where('id_persona', $data['datos']['persona_id'])->update('personas', $data_persona);
		$data_medico = array (
							'especialidad_id'	=> $data['datos']['especialidad_id'],
							'primeras_medico'	=> $data['datos']['primeras_medico'],
							'controles_medico'	=> $data['datos']['controles_medico'],
							'reposos_medico'	=> $data['datos']['reposos_medico'],
							'cedula_medico'		=> strtoupper($data['datos']['cedula_medico']),
							'MPPSPS_medico'		=> strtoupper($data['datos']['MPPSPS_medico']),
							'turno_medico'		=> $data['datos']['turno_medico'],
							'horario_medico'	=> $data['datos']['horario_medico'],
							'condicion_medico'	=> $data['datos']['condicion_medico']
						);
		$med = $this->db->where('id_medico', $data['datos']['id_medico'])->update('medicos', $data_medico);
		if ($per) {
			if ($med) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function destroy($data)
	{
		$per = $this->db->where('id_medico', $data['id'])->delete('medicos');
		if ($per) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
