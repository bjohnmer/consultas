<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Modificar Médico</h2>
        <a href="<?=base_url()?>index.php/medicos" class="btn btn-inverse"><i class='icon-chevron-left icon-white'></i> Ir a la Lista</a>
        

        <form class="form-horizontal" action="<?=base_url()?>index.php/medicos/edit" method="post">
          <fieldset>
            
            <?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
            <?php if (!empty($mensaje)) echo "<div class='alert alert-error'><p>".$mensaje."</p></div>";?>
            <div class="row-fluid">
              <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Cédula</label>
                      <div class="controls docs-input-sizes">
                        <input name="cedula_medico" class="span5" type="text" placeholder="Cédula" value="<?=$consulta[0]->cedula_medico?>">
                        <input name="persona_id" type="hidden" value="<?=$consulta[0]->persona_id?>">
                        <input name="id_medico" type="hidden" value="<?=$consulta[0]->id_medico?>">
                        <span class="label">V-12345678</span>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Nombres</label>
                      <div class="controls docs-input-sizes">
                        <input name="nombres_persona" class="span5" type="text" placeholder="Nombres" value="<?=$consulta[0]->nombres_persona?>">
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Apellidos</label>
                      <div class="controls docs-input-sizes">
                        <input name="apellidos_persona" class="span5" type="text" placeholder="Apellidos" value="<?=$consulta[0]->apellidos_persona?>">
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Dirección</label>
                      <div class="controls docs-input-sizes">
                        <textarea name="direccion_persona" class="input-xlarge" id="textarea" rows="3"><?=$consulta[0]->direccion_persona?></textarea>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Sexo</label>
                      <div class="controls">
                        <label class="radio">
                          <input type="radio" name="sexo_persona" id="sexo_persona" value="Masculino" <?php if($consulta[0]->sexo_persona == 'Masculino') echo 'checked="checked"' ?>>
                          Masculino
                        </label>
                        <label class="radio">
                          <input type="radio" name="sexo_persona" id="sexo_persona" value="Femenino" <?php if($consulta[0]->sexo_persona == 'Femenino') echo 'checked="checked"' ?>>
                          Femenino
                        </label>
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Turno</label>
                      <div class="controls">
                        <label class="radio">
                          <input type="radio" name="turno_medico" id="turno_medico" value="Mañana" <?php if($consulta[0]->turno_medico == 'Mañana') echo 'checked="checked"' ?>>
                          Mañana
                        </label>
                        <label class="radio">
                          <input type="radio" name="turno_medico" id="turno_medico" value="Tarde" <?php if($consulta[0]->turno_medico == 'Tarde') echo 'checked="checked"' ?>>
                          Tarde
                        </label>
                      </div>
                    </div>
              </div>

              <div class="span6">
                  
                <div class="control-group">
                  <label class="control-label">Nacionalidad</label>
                  <div class="controls docs-input-sizes">
                    <input name="nacionalidad_persona" class="span4" type="text" placeholder="Nacionalidad" value="<?=$consulta[0]->nacionalidad_persona?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Especialidad</label>
                  <div class="controls docs-input-sizes">
                    <select name="especialidad_id" class="span5">
                      <?php if($records) : ?>
                        <?php  foreach($records as $row) : ?>
                      <option value="<?=$row->id_especialidad?>" <?php if($consulta[0]->especialidad_id == $row->id_especialidad) echo 'selected="selected"' ?>><?=$row->nombre_especialidad?></option>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </select>
                    <!--p class="help-block">Use the same <code>.span*</code> classes from the grid system for input sizes.</p-->
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Primeras Cítas por día</label>
                  <div class="controls docs-input-sizes">
                    <input name="primeras_medico" class="span1" type="text" placeholder="Primeras Cítas por día" value="<?=$consulta[0]->primeras_medico?>">
                  </div>
                </div>
                    
                <div class="control-group">
                  <label class="control-label">Controles por día</label>
                  <div class="controls docs-input-sizes">
                    <input name="controles_medico" class="span1" type="text" placeholder="Controles por día" value="<?=$consulta[0]->controles_medico?>">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Reposos por día</label>
                  <div class="controls docs-input-sizes">
                    <input name="reposos_medico" class="span1" type="text" placeholder="Reposos por día" value="<?=$consulta[0]->reposos_medico?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Código MPPSPS</label>
                  <div class="controls docs-input-sizes">
                    <input name="MPPSPS_medico" class="span5" type="text" placeholder="Código MPPSPS" value="<?=$consulta[0]->MPPSPS_medico?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Horario de Atención</label>
                  <div class="controls docs-input-sizes">
                    <input name="horario_medico" class="span5" type="text" placeholder="Horario de Atención" value="<?=$consulta[0]->horario_medico?>">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Condición</label>
                  <div class="controls">
                    <label class="radio">
                      <input type="radio" name="condicion_medico" id="condicion_medico" value="Fijo" <?php if($consulta[0]->condicion_medico == 'Fijo') echo 'checked="checked"' ?>>
                      Fijo
                    </label>
                    <label class="radio">
                      <input type="radio" name="condicion_medico" id="condicion_medico" value="Contratado" <?php if($consulta[0]->condicion_medico == 'Contratado') echo 'checked="checked"' ?>>
                      Contratado
                    </label>
                    <label class="radio">
                      <input type="radio" name="condicion_medico" id="condicion_medico" value="Suplente" <?php if($consulta[0]->condicion_medico == 'Suplente') echo 'checked="checked"' ?>>
                      Suplente
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-primary">Modificar</button>
              <button type="reset" class="btn">Limpiar</button>
            </div>


          </fieldset>
        </form>

      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
</body>
</html>
