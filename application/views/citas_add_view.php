<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/blitzer/jquery-ui-1.8.21.custom.css"
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Nueva Cita</h2>
        <a href="<?=base_url()?>index.php/citas" class="btn btn-inverse"><i class='icon-chevron-left icon-white'></i> Ir a la Lista</a>
        

        <form class="form-horizontal" action="<?=base_url()?>index.php/citas/add" method="post">
          <fieldset>
            
            <?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
            <?php if (!empty($mensaje)) echo "<div class='alert alert-error'><p>".$mensaje."</p></div>";?>
            <div class="row-fluid">
              <div class="span6">
                    
                    <div class="control-group">
                      <label class="control-label">Médico</label>
                      <div class="controls docs-input-sizes">
                        <select name="medico_id" class="span7">
                          <?php if($records) : ?>
                            <?php  foreach($records as $row) : ?>
                          <option value="<?=$row->id_medico?>"><?=$row->nombres_persona?> <?=$row->apellidos_persona?> | <?=$row->nombre_especialidad?></option>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </select>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Paciente</label>
                      <div class="controls docs-input-sizes">
                        <select name="id_historia" class="span7">
                          <?php if($historias) : ?>
                            <?php  foreach($historias as $row) : ?>
                          <option value="<?=$row->id_historia?>.<?=$row->id_paciente?>"><?=$row->num_historia?> | <?php

                            $data['historias'] = $this->pacientes->getreg($row->paciente_id);
                
                            $data2['historias'] = $this->pacadulto->getreg($row->paciente_id);

                            if (!empty($data2['historias'][0]->cedula_paciente)) {
                              echo $data2['historias'][0]->cedula_paciente."&nbsp;";
                            }
                            echo $data['historias'][0]->nombres_persona . "&nbsp;" . $data['historias'][0]->apellidos_persona;

                          ?></option>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </select>
                      </div>
                    </div>

                    <div class="control-group">
                      <label class="control-label">Fecha</label>
                      <div class="controls docs-input-sizes">
                        <input name="fecha" id="fecha" class="span3" type="text" value="" readonly>
                      </div>
                    </div>

                  </div>

              </div>


            <div class="form-actions">
              <button type="submit" class="btn btn-primary">Guardar</button>
              <button type="reset" class="btn">Limpiar</button>
            </div>


          </fieldset>
        </form>

      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
  <script type="text/javascript">
      $(function(){
        // Datepicker
        $('#fecha').datepicker({
          inline: true,
          dateFormat    : 'yy-mm-dd',
          dayNames      : ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
          dayNamesMin   : ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
          dayNameShort  : ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
          monthNames    : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
          changeMonth   : true,
          changeYear    : true
        });

      });
  </script>
</body>
</html>
