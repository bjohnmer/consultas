<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historias_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall($num,$offset)
	{
		$consulta = $this->db->join('pacientes', 'pacientes.id_paciente = historias.paciente_id')->get('historias', $num, $offset);

		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function find($data, $num, $offset)
	{
		$sql = "SELECT historias.*, personas.*, especialidades.* FROM historias JOIN personas ON historias.persona_id = personas.id_persona JOIN especialidades ON historias.especialidad_id = especialidades.id_especialidad WHERE personas.nombres_persona LIKE ?";
		
		$consulta = $this->db->query($sql, '%'.mysql_real_escape_string($data['texto']).'%');

		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getallnp()
	{
		$consulta = $this->db->get('historias');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getreg($id)
	{
		$consulta = $this->db->where("id_historia", $id)->get('historias');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getTotalCount()
	{
		$consulta = $this->db->get('historias');
		if ($consulta->num_rows()) {
			$data = $consulta->num_rows();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function create($data)
	{
		/*
			Captura y almacenamiento de datos en las tablas
		*/
		$data_persona = array (
							'nombres_persona'		=> $data['nombres_persona'],
							'apellidos_persona'		=> $data['apellidos_persona'],
							'direccion_persona'		=> $data['direccion_persona'],
							'sexo_persona'			=> $data['sexo_persona'],
							'nacionalidad_persona'	=> $data['nacionalidad_persona']
						);

		$per = $this->db->insert('personas', $data_persona);
		
		if ($per) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		$data_paciente = array (
							'persona_id'	=> $this->db->insert_id(),
							'fn_paciente'	=> $data['fn_paciente'],
						);
		if (!empty($data['asegurado_paciente'])) $data_paciente['asegurado_paciente'] = $data['asegurado_paciente'];
		if (!empty($data['beneficiario_paciente'])) $data_paciente['beneficiario_paciente'] = $data['beneficiario_paciente'];
		
		$pac = $this->db->insert('pacientes', $data_paciente);

		if ($pac) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


		$data_historia = array (
							'num_historia'	=> $data['num_historia'],
							'paciente_id'	=> $this->db->insert_id(),
							'tipo_historia'	=> $data['tipo_historia']
						);
		$his = $this->db->insert('historias', $data_historia);

		if ($his) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


		if (!empty($data['is_adulto'])) {
			$data_adulto = array (
							'paciente_id' => $data_historia['paciente_id'],
							'cedula_paciente' => $data['cedula_paciente']
						);
			$adu = $this->db->insert('pacadulto', $data_adulto);
			
			if ($adu) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		} else {
			$data_persona_rep = array (
								'nombres_persona'		=> $data['nombres_persona_rep'],
								'apellidos_persona'		=> $data['apellidos_persona_rep'],
								'direccion_persona'		=> $data['direccion_persona_rep'],
								'sexo_persona'			=> $data['sexo_persona_rep'],
								'nacionalidad_persona'	=> $data['nacionalidad_persona_rep']
								);
			$rep = $this->db->insert('personas', $data_persona_rep);

			$data_rep = array (
								'persona_id'				=> $this->db->insert_id(),
								'edad_representante'		=> $data['edad_representante'],
								'cedula_representante'		=> $data['cedula_representante'],
								'parentesco_representante'	=> $data['parentesco_representante']
								);
			$rep = $this->db->insert('representantes', $data_rep);
			
			if ($rep) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

			$data_nino = array (
								'paciente_id'		=> $data_historia['paciente_id'],
								'representante_id'	=> $this->db->insert_id()
							);
			$nin = $this->db->insert('pacnino', $data_nino);
			
			if ($nin) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		}

		if (in_array("Er",$ok)) { return FALSE; } else { return TRUE; }

	}

	public function update($data)
	{
		/*
			Captura y modificación de datos en las tablas
		*/
		$data_persona = array (
							'nombres_persona'		=> $data['nombres_persona'],
							'apellidos_persona'		=> $data['apellidos_persona'],
							'direccion_persona'		=> $data['direccion_persona'],
							'sexo_persona'			=> $data['sexo_persona'],
							'nacionalidad_persona'	=> $data['nacionalidad_persona']
						);

		$per = $this->db->where('id_persona', $data['datos']['id_persona'])->update('personas', $data_persona);
		
		if ($per) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		$data_paciente = array (
							'persona_id'	=> $this->db->insert_id(),
							'fn_paciente'	=> $data['fn_paciente'],
						);
		if (!empty($data['asegurado_paciente'])) $data_paciente['asegurado_paciente'] = $data['asegurado_paciente'];
		if (!empty($data['beneficiario_paciente'])) $data_paciente['beneficiario_paciente'] = $data['beneficiario_paciente'];
		
		$pac = $this->db->where('id_paciente', $data['datos']['id_paciente'])->update('pacientes', $data_paciente);

		if ($pac) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


		$data_historia = array (
							'num_historia'	=> $data['num_historia'],
							'paciente_id'	=> $this->db->insert_id(),
							'tipo_historia'	=> $data['tipo_historia']
						);
		$his = $this->db->where('id_historia', $data['datos']['id_historia'])->update('historias', $data_historia);

		if ($his) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


		if (!empty($data['is_adulto'])) {
			$data_adulto = array (
							'paciente_id' => $data_historia['paciente_id'],
							'cedula_paciente' => $data['cedula_paciente']
						);
			$adu = $this->where('id_pacadulto', $data['datos']['id_pacadulto'])->db->update('pacadulto', $data_adulto);
			
			if ($adu) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		} else {
			$data_persona_rep = array (
								'persona_id'				=> $data_paciente['persona_id'],
								'edad_representante'		=> $data['edad_representante'],
								'cedula_representante'		=> $data['cedula_representante'],
								'parentesco_representante'	=> $data['parentesco_representante']
								);
			$rep = $this->db->where('id_representante', $data['datos']['id_representante'])->update('representantes', $data_persona_rep);
			
			if ($rep) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

			$data_nino = array (
								'paciente_id'		=> $data_historia['paciente_id'],
								'representante_id'	=> $this->db->insert_id()
							);
			$nin = $this->db->where('id_pacnino', $data['datos']['id_pacnino'])->update('pacnino', $data_nino);
			
			if ($nin) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

		}

		if (in_array("Er",$ok)) { return FALSE; } else { return TRUE; }

		/*
		$data_persona = array (
							'nombres_persona'		=> $data['datos']['nombres_persona'],
							'apellidos_persona'		=> $data['datos']['apellidos_persona'],
							'direccion_persona'		=> $data['datos']['direccion_persona'],
							'sexo_persona'			=> $data['datos']['sexo_persona'],
							'nacionalidad_persona'	=> $data['datos']['nacionalidad_persona']
						);
		
		$per = $this->db->where('id_persona', $data['datos']['persona_id'])->update('personas', $data_persona);
		$data_medico = array (
							'especialidad_id'	=> $data['datos']['especialidad_id'],
							'primeras_medico'	=> $data['datos']['primeras_medico'],
							'controles_medico'	=> $data['datos']['controles_medico'],
							'reposos_medico'	=> $data['datos']['reposos_medico'],
							'cedula_medico'		=> strtoupper($data['datos']['cedula_medico']),
							'MPPSPS_medico'		=> strtoupper($data['datos']['MPPSPS_medico']),
							'condicion_medico'	=> $data['datos']['condicion_medico']
						);
		$med = $this->db->where('id_medico', $data['datos']['id_medico'])->update('historias', $data_medico);
		if ($per) {
			if ($med) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}*/
	}

	function destroy($data)
	{
		$per = $this->db->where('id_medico', $data['id'])->delete('historias');
		if ($per) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
