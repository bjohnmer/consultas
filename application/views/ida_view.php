<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/blitzer/jquery-ui-1.8.21.custom.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Informe Diario de Actividades</h2>
        <h4>Seleccione Una Fecha</h4>
        
        <form class="form-horizontal" action="<?=base_url()?>ida/consulta" method="post">
          <fieldset>
            
            <?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
            <?php if (!empty($mensaje)) echo "<div class='alert alert-error'><p>".$mensaje."</p></div>";?>
            <div class="row-fluid">
              <div class="span6">
                    
                    <div class="control-group">
                      <label class="control-label">Fecha</label>
                      <div class="controls docs-input-sizes">
                        <input name="fecha" id="fecha" class="span3" type="text" value="" readonly>
                      </div>
                    </div>

                  </div>

              </div>


            <div class="form-actions">
              <button type="submit" class="btn btn-primary">Guardar</button>
              <button type="reset" class="btn">Limpiar</button>
            </div>


          </fieldset>
        </form>

      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
  <script type="text/javascript">
      $(function(){
        // Datepicker
        $('#fecha').datepicker({
          inline: true,
          dateFormat    : 'yy-mm-dd',
          dayNames      : ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
          dayNamesMin   : ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
          dayNameShort  : ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
          monthNames    : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
          changeMonth   : true,
          changeYear    : true
        });

      });
  </script>
</body>
</html>
