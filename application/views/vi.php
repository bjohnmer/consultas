<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo"><h1>Visión</h1></div>
    <div id="columcont">
      <p>
        Fortalecerse como una Institución Pública eficiente, con la capacidad de establecer control en sus actuaciones y que su trabajo sea mostrado, de acuerdo a las actividades que se realizan allí y mantener de esta manera la credibilidad de la legalidad, exactitud y sinceridad, para revelar de esta forma la transparencia y responsabilidad, de su gestión.
    </p>
        <p>
        Esta Institución mediante la inspiración de la justicia social y de la equidad, garantizara el cumplimiento de los principios y normas de Seguridad Social, a todos los Beneficiarios del país y con la calidad de excelencia en los servicios prestados.
    </p>
    </div>
    <div id="columimg">
      <img src="<?=base_url()?>img/hospital_belen.jpg">
    </div>
</div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
