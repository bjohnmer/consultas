<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ida extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('pagination');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		} else {
			$this->load->model("pacientes_model","pacientes");
			$this->load->model("pacadulto_model","pacadulto");
			$this->load->model("pacnino_model","pacnino");
			$this->load->model("representantes_model","representantes");
			$this->load->model("historias_model","historias");
			$this->load->model("medicos_model","medicos");
			$this->load->model("citas_model","citas");
		}
	}
	function getPageConfig()
	{
		$config['base_url'] = base_url().'index.php/historias/index';
		$config['total_rows'] = $this->historias->getTotalCount();
		$config['per_page'] = 20;
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		return $config;
	}
	public function index() {

		// $data['records'] = $this->citas->getall($config['per_page'],$this->uri->segment(3));

		$this->load->view('ida_view');

	}

	// public function add_form($data = array()) {
	// 	$data['records'] = $this->medicos->getallnpt();
	// 	$data['historias'] = $this->historias->getallnp();
	// 	$this->load->view('citas_add_view', $data);			
	// }


	public function consulta() 
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('fecha', 'Fecha', 'required|xss_clean');
		
		if ($this->form_validation->run() == TRUE)
		{
			$respuesta = $this->citas->getallfecha($_POST['fecha']);

			if ($respuesta) {

				$this->load->view('ida_report', $respuesta);

			} else {
				
				$data['mensaje'] = 'Error: No se han encontrado registros en ésta fecha';
				
				$this->index($data);
				
			}
		}

	}

	// public function delete() 
	// {
		
	// 	$data['id'] = $this->uri->segment(3);
	// 	$respuesta = $this->citas->destroy($data);
	// 	if ($respuesta) {
	// 		$data["mensaje"] = "El registro se ha eliminado";
			
	// 		$config = $this->getPageConfig();

	// 		$this->pagination->initialize($config);
	// 		$data['records'] = $this->citas->getall($config['per_page'],'');
			
	// 		$this->load->view('citas_view', $data);
	// 	} else {
	// 		$data['mensaje'] = 'Error: No se ha podido eliminar';
	// 		$this->edit_form($data);
	// 	}
	// }


	// public function find() 
	// {
	// 	$config = $this->getPageConfig();
	// 	$data = $_POST;
	// 	$data['records']= $this->historias->find($data,$config['per_page'],'');
	// 	if ($data['records']) {
						
	// 		$this->pagination->initialize($config);
			
	// 		$this->load->view('historias_view', $data);
	// 	} else {
	// 		$data['mensaje'] = "Error en la búsqueda";
	// 		$this->load->view('historias_view', $data);
	// 	}
	// }


}