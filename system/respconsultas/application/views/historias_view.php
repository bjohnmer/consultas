<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css/admin.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Historias</h2>
        <a href="<?=base_url()?>index.php/historias/add_form" class="btn btn-primary"><i class='icon-plus icon-white'></i> Nueva Historia</a>
        <?=$this->pagination->create_links();?>
        <?php if (!empty($mensaje)) echo "<br><br><div class='alert alert-success'><p>".$mensaje."</p></div>";?>
        <?php if($records) : ?>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>N° de Historia</th>
              <th>Paciente</th>
              <th>Asegurado</th>
              <th>Beneficiario</th>
              <th>Fecha de nacimiento</th>
              <th>Tipo de Historia</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <?php  foreach($records as $row) : ?>
            <tr>
              <td><?=$row->num_historia?></td>
              <td><?php
                $data['records'] = $this->pacientes->getreg($row->paciente_id);
                
                $data2['records'] = $this->pacadulto->getreg($row->paciente_id);

                if (!empty($data2['records'][0]->cedula_paciente)) {
                  echo $data2['records'][0]->cedula_paciente."&nbsp;";
                }
                echo $data['records'][0]->nombres_persona . "&nbsp;" . $data['records'][0]->apellidos_persona;
                
                ?></td>
              <td><?=$row->asegurado_paciente?></td>
              <td><?=$row->beneficiario_paciente?></td>
              <td><?=$row->fn_paciente?></td>
              <td><?=$row->tipo_historia?></td>
              <!--td>
              <?php 
                /*if (isset($row->cedula_pacadulto)) 
                { 
                  echo $row->cedula_pacadulto;
                }*/
              ?>&nbsp;
              <?php //=$row->nombres_persona;?>&nbsp;
              <?php //=$row->apellidos_persona;?>
              </td-->
              <td><?=anchor(base_url()."index.php/historias/edit_form/".$row->id_historia, "<i class='icon-edit'></i> Modificar")?></td>
              <td><?=anchor(base_url()."index.php/historias/delete/".$row->id_historia, "<i class='icon-trash'></i> Eliminar", "class='eliminar'")?></td>
            </tr>
          <?php endforeach; ?>
          </table>
        <?=$this->pagination->create_links();?>
    	 <?php else : ?>	
    	         <h2>No hay registros para mostrar</h2>
    	 <?php endif; ?>
      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
</body>
</html>
