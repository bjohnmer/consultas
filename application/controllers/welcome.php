<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('Form_validation');
		//$this->config->set_item('language', 'spanish');
	}
	public function index()
	{
		$this->load->library('session');
		$this->load->view('welcome');
	}
	public function rh()
	{
		$this->load->view('rh');
	}
	public function mi()
	{
		$this->load->view('mi');
	}
	public function vi()
	{
		$this->load->view('vi');
	}
	public function ia()
	{
		$this->load->view('ia');
	}
	public function co()
	{
		$this->load->helper('form');
		$this->load->view('co');
	}
	public function send_contact()
	{
		$this->form_validation->set_rules("nombre", "Nombre", "required|alpha_space|xss_clean");
		$this->form_validation->set_rules("email", "Correo Electrónico", "required|valid_email|xss_clean");
		$this->form_validation->set_rules("comentario", "Comentario", "required|xss_clean");

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('co');
		} else {
			$this->load->library("email");
			$this->email->from(set_value("email"),set_value("nombre"));
			$this->email->to("bjohnmer@gmail.com");
			$this->email->subject("Comentario desde el Sistema de Consultas IVSS");
			$this->email->message(set_value("comentario"));
			if ($this->email->send()){
				$data["mensaje"] = "El correo se ha enviado correctamente";
				$this->load->helper('form');
				$this->load->view('co', $data);
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */