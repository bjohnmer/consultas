-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 04, 2012 at 06:53 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `consultas`
--

-- --------------------------------------------------------

--
-- Table structure for table `especialidades`
--

CREATE TABLE IF NOT EXISTS `especialidades` (
  `idespecialidades` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idespecialidades`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `especialidades`
--

INSERT INTO `especialidades` (`idespecialidades`, `nombre`) VALUES
(1, 'Neurología'),
(2, 'Fisiología'),
(3, 'Traumatología');

-- --------------------------------------------------------

--
-- Table structure for table `historias`
--

CREATE TABLE IF NOT EXISTS `historias` (
  `idhistorias` int(11) NOT NULL AUTO_INCREMENT,
  `n_historia` varchar(45) DEFAULT NULL,
  `idpaciente` int(11) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idhistorias`),
  UNIQUE KEY `n_historia_UNIQUE` (`n_historia`),
  UNIQUE KEY `idpaciente_UNIQUE` (`idpaciente`),
  KEY `fk_historias_paciente` (`idpaciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `medicos`
--

CREATE TABLE IF NOT EXISTS `medicos` (
  `idmedicos` int(11) NOT NULL AUTO_INCREMENT,
  `idpersona` int(11) DEFAULT NULL,
  `idespecialidad` int(11) DEFAULT NULL,
  `primera` int(11) DEFAULT NULL,
  `control` int(11) DEFAULT NULL,
  `reposos` int(11) DEFAULT NULL,
  `cedula` varchar(15) DEFAULT NULL,
  `MPPSPS` varchar(45) DEFAULT NULL,
  `condicion` enum('Fijo','Contratado','Suplente') DEFAULT NULL,
  PRIMARY KEY (`idmedicos`),
  UNIQUE KEY `id_persona_UNIQUE` (`idpersona`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  KEY `fk_medicos_persona` (`idespecialidad`),
  KEY `fk_medicos_especialidad` (`idpersona`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `medicos`
--

INSERT INTO `medicos` (`idmedicos`, `idpersona`, `idespecialidad`, `primera`, `control`, `reposos`, `cedula`, `MPPSPS`, `condicion`) VALUES
(3, 2, 1, 5, 5, 5, '14460452', '123123asdsa', 'Fijo'),
(6, 3, 2, 5, 5, 5, 'V-12345678', 'UWYTER65W67R', 'Contratado'),
(7, 4, 2, 5, 5, 5, 'V-32216548', 'IUWEYR76', 'Suplente');

-- --------------------------------------------------------

--
-- Table structure for table `pacadulto`
--

CREATE TABLE IF NOT EXISTS `pacadulto` (
  `idpacadulto` int(11) NOT NULL AUTO_INCREMENT,
  `idpacientes` int(11) DEFAULT NULL,
  `cedula` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`idpacadulto`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  KEY `fk_pacadulto_paciente` (`idpacientes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pacientes`
--

CREATE TABLE IF NOT EXISTS `pacientes` (
  `idpacientes` int(11) NOT NULL AUTO_INCREMENT,
  `id_personas` int(11) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `asegurado` enum('Si','No') DEFAULT NULL,
  `beneficiario` enum('Si','No') DEFAULT NULL,
  PRIMARY KEY (`idpacientes`),
  UNIQUE KEY `id_persona_UNIQUE` (`id_personas`),
  KEY `fk_paciente_persona` (`id_personas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pacientes`
--

INSERT INTO `pacientes` (`idpacientes`, `id_personas`, `fecha_nac`, `asegurado`, `beneficiario`) VALUES
(1, 2, '1980-06-18', 'No', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `pacnino`
--

CREATE TABLE IF NOT EXISTS `pacnino` (
  `idpacnino` int(11) NOT NULL AUTO_INCREMENT,
  `idpacientes` int(11) DEFAULT NULL,
  `idrepresentantes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpacnino`),
  UNIQUE KEY `idrepresentantes_UNIQUE` (`idrepresentantes`),
  UNIQUE KEY `idpacientes_UNIQUE` (`idpacientes`),
  KEY `fk_pacnino_paciente` (`idpacientes`),
  KEY `fk_pacnino_representante` (`idrepresentantes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `idpersonas` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `direccion` text,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpersonas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`idpersonas`, `nombres`, `apellidos`, `direccion`, `sexo`, `nacionalidad`) VALUES
(2, 'Johnmer', 'Bencomo', '<p>Valera, Plata IV, Calle 1</p>', 'Masculino', 'Venezolana'),
(3, 'Alberto', 'Calderas', 'Valera Estado Trujillo', 'Masculino', 'Venezolana'),
(4, 'Juan', 'Daza', 'Caracas, Venezuela', 'Masculino', 'Venezolana');

-- --------------------------------------------------------

--
-- Table structure for table `representantes`
--

CREATE TABLE IF NOT EXISTS `representantes` (
  `idrepresentantes` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonas` int(11) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `cedula` varchar(15) DEFAULT NULL,
  `parentesco` enum('Padre','Madre','Abuel@','Tí@','Herman@','Prim@') DEFAULT NULL,
  PRIMARY KEY (`idrepresentantes`),
  UNIQUE KEY `idpersonas_UNIQUE` (`idpersonas`),
  KEY `fk_representante_1` (`idpersonas`),
  KEY `fk_representantes_persona` (`idpersonas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `nivel` enum('Administrador','Supervisor','Limitado') DEFAULT 'Administrador',
  `usuario` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `apellido`, `cargo`, `nivel`, `usuario`, `clave`) VALUES
(1, 'Administrador', 'Administrador', 'Administrador', 'Administrador', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `historias`
--
ALTER TABLE `historias`
  ADD CONSTRAINT `fk_historias_paciente` FOREIGN KEY (`idpaciente`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `medicos`
--
ALTER TABLE `medicos`
  ADD CONSTRAINT `fk_medicos_especialidad` FOREIGN KEY (`idespecialidad`) REFERENCES `especialidades` (`idespecialidades`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_medicos_persona` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersonas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pacadulto`
--
ALTER TABLE `pacadulto`
  ADD CONSTRAINT `fk_pacadulto_paciente` FOREIGN KEY (`idpacientes`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pacientes`
--
ALTER TABLE `pacientes`
  ADD CONSTRAINT `fk_paciente_persona` FOREIGN KEY (`id_personas`) REFERENCES `personas` (`idpersonas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pacnino`
--
ALTER TABLE `pacnino`
  ADD CONSTRAINT `fk_pacnino_paciente` FOREIGN KEY (`idpacientes`) REFERENCES `pacientes` (`idpacientes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pacnino_representante` FOREIGN KEY (`idrepresentantes`) REFERENCES `representantes` (`idrepresentantes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `representantes`
--
ALTER TABLE `representantes`
  ADD CONSTRAINT `fk_representantes_persona` FOREIGN KEY (`idpersonas`) REFERENCES `personas` (`idpersonas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
