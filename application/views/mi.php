<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php require_once("head.php");?>
</head>
<body>
<div class="gridContainer clearfix">
  <?php require_once("top.php");?>
  <div id="content">
    <div id="titulo"><h1>Misión</h1></div>
    <div id="columcont">
      <p>
        El Instituto Venezolano de los Seguros Sociales del Estado Trujillo, es una institución pública, al que corresponde brindar protección de la seguridad social a todos los beneficiarios, además el control, la vigilancia, presupuesto, y evaluación financiera de los ingresos, gastos, y bienes que allí ingresan, así como de las operaciones relativas a los, cuyas actuaciones se orientaran a la realización tramitaciones, cotizaciones, conciliaciones, ordenes, Instituto Venezolano de seguro Sociales, en el ejercicio de sus funciones, verificara la legalidad, exactitud y sinceridad así como la eficacia, economía eficiencia, calidad e impacto de las operaciones y de los resultados de la gestión de su organización.
    </p>
    </div>
    <div id="columimg">
      <img src="<?=base_url()?>img/cumana.jpg">
    </div>
    
  </div>
  <?php require_once("footer.php");?>
</div>
</body>
</html>
