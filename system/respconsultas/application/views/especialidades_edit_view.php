<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css/admin.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Modificar Especialidad</h2>
        <a href="<?=base_url()?>index.php/especialidades" class="btn btn-inverse"><i class='icon-chevron-left icon-white'></i> Ir a la Lista</a>
        

        <form class="form-horizontal" action="<?=base_url()?>index.php/especialidades/edit" method="post">
          <fieldset>
            <?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
            <?php if (!empty($mensaje)) echo "<div class='alert alert-error'><p>".$mensaje."</p></div>";?>
            <div class="row-fluid">
              <div class="span6">
                    <div class="control-group">
                      <label class="control-label">Nombre</label>
                      <div class="controls docs-input-sizes">
                        <input name="nombre_especialidad" class="span5" type="text" placeholder="Nombre" value="<?=$consulta[0]->nombre_especialidad?>">
                        <input name="id_especialidad" type="hidden" value="<?=$consulta[0]->id_especialidad?>">
                      </div>
                    </div>
              </div>
            </div>

            <div class="form-actions">
              <button type="submit" class="btn btn-primary">Modificar</button>
              <button type="reset" class="btn">Limpiar</button>
            </div>
          </fieldset>
        </form>

      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
</body>
</html>
