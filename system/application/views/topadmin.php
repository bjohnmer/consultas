<div class="navbar">
    <div class="navbar-inner dgazul">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="#">Menú Principal</a>
        <div class="nav-collapse">
          <ul class="nav">
            <li class="active"><a href="<?=base_url();?>index.php/admin">Inicio</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Datos Iniciales <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="divider"></li>
                <li class="nav-header">Datos de los Médicos</li>
                <li><a href="<?=base_url();?>index.php/especialidades">Epecialidades</a></li>
                <li><a href="<?=base_url();?>index.php/medicos">Médicos</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Historias Médicas <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="divider"></li>
                <li class="nav-header">Historias</li>
                <li><a href="<?=base_url();?>index.php/historias">Historias Médicas</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Citas <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="divider"></li>
                <li class="nav-header">Datos de los Médicos</li>
                <li><a href="#">Epecialidades</a></li>
                <li><a href="#">Médicos</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav pull-right">
            <li><a href="login/logout">Salir</a></li>
          </ul>
          <form class="navbar-search pull-right" action="<?="/index.php/".$this->uri->segment(1)."/find/"?>" method="post" >
            <input name="texto" type="text" class="search-query span2" placeholder="Buscar">
          </form>
        </div><!-- /.nav-collapse -->
      </div>
    </div><!-- /navbar-inner -->
  </div>
<div id="top">
        <div class="dgazul" id="head2img">
        	<div id="logo">
            	<a href="index.php" alt="Inicio" title="Inicio"><img src="/img/logoivss.fw.png" height="4%" width="4%"></a>
                <div id="tlogo">Instituto Venezolano de los Seguros Sociales</div>
            	  <div id="stlogo">La Seguridad Social es tu Derecho</div>
                <h4>Área de Administración</h4>
            </div>
        </div>
  </div>