<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historias extends CI_Controller {
	function __construct(){
		parent::__construct();
		//$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('pagination');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		} else {
			$this->load->model("pacientes_model","pacientes");
			$this->load->model("pacadulto_model","pacadulto");
			$this->load->model("pacnino_model","pacnino");
			$this->load->model("representantes_model","representantes");
			$this->load->model("historias_model","historias");
		}
	}
	function getPageConfig()
	{
		$config['base_url'] = base_url().'index.php/historias/index';
		$config['total_rows'] = $this->historias->getTotalCount();
		$config['per_page'] = 20;
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		return $config;
	}
	public function index() {

		$config = $this->getPageConfig();
		
		$this->pagination->initialize($config);
		$data['records'] = $this->historias->getall($config['per_page'],$this->uri->segment(3));

		$this->load->view('historias_view', $data);

	}
	public function add_form($data = array()) {
		$this->load->view('historias_add_view', $data);			
	}

	public function edit_form($data = array()) 
	{

		$data = array (	'historia' 	=> $this->historias->getreg($this->uri->segment(3)));
		$data['paciente'] = $this->pacientes->getreg($data['historia'][0]->paciente_id);
		$isAdulto = $this->pacadulto->getreg($data['historia'][0]->paciente_id);

		if (!empty($isAdulto)) {
			$data['adulto'] = $isAdulto;
			$data['adulto']['es_adulto'] = 'Si';
		} else {
			$datanino = $this->pacnino->getreg($data['historia'][0]->paciente_id);
			$data['representante'] = $this->representantes->getreg($datanino[0]->representante_id);
			// print_r($data['representante'][0]);
		}

		$this->load->view('historias_edit_view', $data);

	}

	public function add() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('num_historia', 'N° de Historia', 'trim|required|ss_clean');
		$this->form_validation->set_rules('tipo_historia', 'Tipo', 'trim|alpha_space|required|xss_clean');
		
		if (!empty($_POST['is_adulto'])){
			$this->form_validation->set_rules('cedula_paciente', 'Cédula', 'trim|required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
		} else {
			$this->form_validation->set_rules('cedula_representante', 'Cédula del Representante', 'trim|required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
			$this->form_validation->set_rules('edad_representante', 'Edad del Representante', 'trim|numeric|required|xss_clean');

			$this->form_validation->set_rules('nombres_persona_rep', 'Nombres del Representante', 'trim|alpha_space|required|xss_clean');
			$this->form_validation->set_rules('apellidos_persona_rep', 'Apellidos del Representante', 'trim|alpha_space|required|xss_clean');
			$this->form_validation->set_rules('direccion_persona_rep', 'Dirección del Representante', 'required|min_length[5]|xss_clean');
			$this->form_validation->set_rules('nacionalidad_persona_rep', 'Nacionalidad del Representante', 'required|alpha_space|xss_clean');
		}

		$this->form_validation->set_rules('nombres_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('apellidos_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('direccion_persona', 'Dirección', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('nacionalidad_persona', 'Nacionalidad', 'required|alpha_space|xss_clean');
		
		$this->form_validation->set_rules('fn_paciente', 'Fecha de Nacimiento', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->add_form();
		}
		else
		{
			$respuesta = $this->historias->create($_POST);
			if ($respuesta) {
				$data["mensaje"] = "Los datos se almacenaron correctamente";

				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->historias->getall($config['per_page'],'');
				$this->load->view('historias_view', $data);

			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
				$this->add_form($data);
			}
		}

	}

	public function edit() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('num_historia', 'N° de Historia', 'trim|required|ss_clean');
		$this->form_validation->set_rules('tipo_historia', 'Tipo', 'trim|alpha_space|required|xss_clean');
		
		if (!empty($_POST['is_adulto'])){
			$this->form_validation->set_rules('cedula_paciente', 'Cédula', 'trim|required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
		} else {
			$this->form_validation->set_rules('cedula_representante', 'Cédula del Representante', 'trim|required|regex_match[/[VveE]-[0-9]{6,9}/]|xss_clean');
			$this->form_validation->set_rules('edad_representante', 'Edad del Representante', 'trim|numeric|required|xss_clean');
			$this->form_validation->set_rules('nombres_persona_rep', 'Nombres del Representante', 'trim|alpha_space|required|xss_clean');
			$this->form_validation->set_rules('apellidos_persona_rep', 'Apellidos del Representante', 'trim|alpha_space|required|xss_clean');
			$this->form_validation->set_rules('direccion_persona_rep', 'Dirección del Representante', 'required|min_length[5]|xss_clean');
			$this->form_validation->set_rules('nacionalidad_persona_rep', 'Nacionalidad del Representante', 'required|alpha_space|xss_clean');
		}

		$this->form_validation->set_rules('nombres_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('apellidos_persona', 'Apellidos', 'trim|alpha_space|required|xss_clean');
		$this->form_validation->set_rules('direccion_persona', 'Dirección', 'required|min_length[5]|xss_clean');
		$this->form_validation->set_rules('nacionalidad_persona', 'Nacionalidad', 'required|alpha_space|xss_clean');
		
		$this->form_validation->set_rules('fn_paciente', 'Fecha de Nacimiento', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->edit_form();
		}
		else
		{
			$respuesta = $this->historias->update($_POST);
			if ($respuesta) {
				echo "--------------entra-----------------";
				$data["mensaje"] = "Los datos se almacenaron correctamente";

				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->historias->getall($config['per_page'],'');
				$this->load->view('historias_view', $data);

			} else {
				$data['mensaje'] = 'Error: No se ha podido actualizar';
				$this->edit_form($data);
			}
		}

	}

	public function delete() 
	{
		
		$data['id'] = $this->uri->segment(3);
		$respuesta = $this->historias->destroy($data);
		if ($respuesta) {
			$data["mensaje"] = "El registro se ha eliminado";
			
			$config = $this->getPageConfig();

			$this->pagination->initialize($config);
			$data['records'] = $this->historias->getall($config['per_page'],'');
			
			$this->load->view('historias_view', $data);
		} else {
			$data['mensaje'] = 'Error: No se ha podido eliminar';
			$this->edit_form($data);
		}
	}


	public function find() 
	{
		$config = $this->getPageConfig();
		$data = $_POST;
		$data['records']= $this->historias->find($data,$config['per_page'],'');
		if ($data['records']) {
						
			$this->pagination->initialize($config);
			
			$this->load->view('historias_view', $data);
		} else {
			$data['mensaje'] = "Error en la búsqueda";
			$this->load->view('historias_view', $data);
		}
	}


}