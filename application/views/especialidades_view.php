<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/bootstrap-responsive.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>css/admin.css">
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Especialidades</h2>
        <a href="<?=base_url()?>index.php/especialidades/add_form" class="btn btn-primary"><i class='icon-plus icon-white'></i> Nueva Especialidad</a>
        <?=$this->pagination->create_links();?>
        <?php if (!empty($mensaje)) echo "<br><br><div class='alert alert-success'><p>".$mensaje."</p></div>";?>
        <?php if($records) : ?>
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Nombre</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <?php  foreach($records as $row) : ?>
            <tr>
              <td><?=$row->nombre_especialidad?></td>
              <td><?=anchor(base_url()."index.php/especialidades/edit_form/".$row->id_especialidad, "<i class='icon-edit'></i> Modificar")?></td>
              <td><?=anchor(base_url()."index.php/especialidades/delete/".$row->id_especialidad, "<i class='icon-trash'></i> Eliminar", "class='eliminar'")?></td>
            </tr>
          <?php endforeach; ?>
          </table>
        <?=$this->pagination->create_links();?>
    	 <?php else : ?>	
    	         <h2>No hay registros para mostrar</h2>
    	 <?php endif; ?>
      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
</body>
</html>
