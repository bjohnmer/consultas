<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Especialidades extends CI_Controller {
	function __construct(){
		parent::__construct();
		//$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('pagination');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		} else {
			$this->load->model("especialidades_model","especialidades");
		}
	}
	function getPageConfig()
	{
		$config['base_url'] = base_url().'index.php/especialidades/index';
		$config['total_rows'] = $this->especialidades->getTotalCount();
		$config['per_page'] = 20;
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		return $config;
	}
	public function index() {

		$config = $this->getPageConfig();
		
		$this->pagination->initialize($config);
		$data['records'] = $this->especialidades->getall($config['per_page'],$this->uri->segment(3));

		$this->load->view('especialidades_view', $data);

	}
	public function add_form($data = array()) {

		$this->load->view('especialidades_add_view', $data);			
	}

	public function edit_form($data = array()) 
	{

		$data = array(
				'consulta' 	=> $this->especialidades->getreg($this->uri->segment(3)),
				);
		$this->load->view('especialidades_edit_view', $data);

	}

	public function add() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nombre_especialidad', 'Nombre', 'trim|alpha_space|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->add_form();
		}
		else
		{
			$respuesta = $this->especialidades->create($_POST);
			if ($respuesta) {
				$data["mensaje"] = "Los datos se almacenaron correctamente";

				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->especialidades->getall($config['per_page'],'');
				$this->load->view('especialidades_view', $data);

			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
				$this->add_form($data);
			}
		}

	}

	public function edit() 
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nombre_especialidad', 'Nombre', 'trim|alpha_space|required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$this->edit_form();
		}
		else
		{
			$data['datos'] = $_POST;
			$respuesta = $this->especialidades->update($data);
			if ($respuesta) {
				$data["mensaje"] = "Los datos se modificaron correctamente";
				
				$config = $this->getPageConfig();

				$this->pagination->initialize($config);
				$data['records'] = $this->especialidades->getall($config['per_page'],'');
				
				$this->load->view('especialidades_view', $data);
			} else {
				$data['mensaje'] = 'Error: No se ha podido guardar';
				$this->edit_form($data);
			}
		}

	}

	public function delete() 
	{
		
		$data['id'] = $this->uri->segment(3);
		$respuesta = $this->especialidades->destroy($data);
		if ($respuesta) {
			$data["mensaje"] = "El registro se ha eliminado";
			
			$config = $this->getPageConfig();

			$this->pagination->initialize($config);
			$data['records'] = $this->especialidades->getall($config['per_page'],'');
			
			$this->load->view('especialidades_view', $data);
		} else {
			$data['mensaje'] = 'Error: No se ha podido eliminar';
			$this->edit_form($data);
		}
	}
	
	public function find() 
	{
		$config = $this->getPageConfig();
		$data = $_POST;
		$data['records']= $this->especialidades->find($data,$config['per_page'],'');
		if ($data['records']) {
						
			$this->pagination->initialize($config);
			
			$this->load->view('especialidades_view', $data);
		} else {
			$data['mensaje'] = "Error en la búsqueda";
			$this->load->view('especialidades_view', $data);
		}
	}


}