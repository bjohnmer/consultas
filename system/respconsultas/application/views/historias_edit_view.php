<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
  <?php //require_once("head.php");?>
  <meta charset="utf-8">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css/admin.css">
  <link type="text/css" href="/css/blitzer/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
</head>
<body>
  <?php require_once("topadmin.php");?>
  <div  class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h2>Modificar Historia</h2>
        <a href="<?=base_url()?>index.php/historias" class="btn btn-inverse"><i class='icon-chevron-left icon-white'></i> Ir a la Lista</a>
        

        <form class="form-horizontal" action="<?=base_url()?>index.php/historias/add" method="post">
          <fieldset>
            
            <?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
            <?php if (!empty($mensaje)) echo "<div class='alert alert-error'><p>".$mensaje."</p></div>";?>

            <div class="row-fluid">
              <div class="span6">
                <div class="control-group">
                  <label class="control-label">N° de Historia</label>
                  <div class="controls docs-input-sizes">
                    <input name="num_historia" class="span5" type="text" placeholder="N° de Historia" value="<?=$historia[0]->num_historia?>">
                    <span class="label">XXXX-###</span>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Tipo</label>
                  <div class="controls docs-input-sizes">
                    <input name="tipo_historia" class="span5" type="text" placeholder="Tipo de Histora" value="<?=$historia[0]->tipo_historia?>">
                  </div>
                </div>

                <div class="control-group">
                  <div class="controls">
                    <label class="checkbox inline">
                      <input type="checkbox" name="is_adulto" id="is_adulto" value="Si" <?php 

                        if (!empty($adulto['es_adulto'])) {
                          echo "checked";
                        }

                      ?>> Adulto
                    </label>
                  </div>
                </div>

                <div class="control-group adulto" >
                  <label class="control-label">Cédula del paciente</label>
                  <div class="controls docs-input-sizes">
                    <input name="cedula_paciente" class="span5" type="text" placeholder="Cédula del paciente" value="<?php 

                        if (!empty($adulto['es_adulto'])) {
                          echo $adulto[0]->cedula_paciente;
                        }

                      ?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Nombres del paciente</label>
                  <div class="controls docs-input-sizes">
                    <input name="nombres_persona" class="span5" type="text" placeholder="Nombres del paciente" value="<?=$paciente[0]->nombres_persona?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Apellidos del paciente</label>
                  <div class="controls docs-input-sizes">
                    <input name="apellidos_persona" class="span5" type="text" placeholder="Apellidos del paciente" value="<?=$paciente[0]->apellidos_persona?>">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Dirección</label>
                  <div class="controls docs-input-sizes">
                    <textarea name="direccion_persona" class="input-xlarge" id="textarea" rows="3"><?=$paciente[0]->direccion_persona?></textarea>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="control-group">
                  <label class="control-label">Sexo</label>
                  <div class="controls">
                    <label class="radio">
                    <input type="radio" name="sexo_persona" id="sexo_persona" value="Masculino" <?php
                      if ($paciente[0]->sexo_persona == "Masculino") {
                        echo 'checked="checked"';
                      }?>>
                        Masculino
                    </label>
                    <label class="radio">
                      <input type="radio" name="sexo_persona" id="sexo_persona" value="Femenino" <?php
                      if ($paciente[0]->sexo_persona == "Femenino") {
                        echo 'checked="checked"';
                      }?>>
                        Femenino
                    </label>
                  </div>
                </div>
                  
                <div class="control-group">
                  <label class="control-label">Nacionalidad</label>
                  <div class="controls docs-input-sizes">
                    <input name="nacionalidad_persona" class="span4" type="text" placeholder="Nacionalidad" value="<?=$paciente[0]->nacionalidad_persona?>">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Fecha de Nacimiento</label>
                  <div class="controls docs-input-sizes">
                    <input name="fn_paciente" id="fn_paciente" class="span3" type="text" value="<?=$paciente[0]->fn_paciente?>" readonly>
                  </div>
                </div>

                <div class="control-group">
                  <div class="controls">
                    <label class="checkbox inline">
                      <input type="checkbox" name="asegurado_paciente" id="asegurado_paciente" value="Si" <?php
                        if ($paciente[0]->asegurado_paciente == "Si") {
                          echo "checked";
                        }
                      ?>> Asegurado
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox" name="beneficiario_paciente" id="beneficiario_paciente" value="Si" <?php
                        if ($paciente[0]->beneficiario_paciente == "Si") {
                          echo "checked";
                        }
                      ?>> Beneficiario
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <!--Representante -->

            <div class="row-fluid nino">
              <div class="span12 well">
                <div class="span6">
                  <h3>Representante</h3>
                    <div class="control-group" id="if_adulto" >
                      <label class="control-label">Cédula</label>
                      <div class="controls docs-input-sizes">
                        <input name="cedula_representante" class="span5" type="text" placeholder="Cédula del Representante" value="<?=$representante[0]->cedula_representante?>">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Nombres</label>
                      <div class="controls docs-input-sizes">
                        <input name="nombres_persona_rep" class="span5" type="text" placeholder="Nombres del Representante" value="<?=$representante[0]->nombres_persona?>">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Apellidos</label>
                      <div class="controls docs-input-sizes">
                        <input name="apellidos_persona_rep" class="span5" type="text" placeholder="Apellidos del Representante" value="<?=$representante[0]->apellidos_persona?>">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Dirección</label>
                      <div class="controls docs-input-sizes">
                        <textarea name="direccion_persona_rep" class="input-xlarge" id="textarea" rows="3"><?=$representante[0]->direccion_persona?></textarea>
                      </div>
                    </div>
                </div>
                <div class="span6">
                  <div class="control-group">
                    <label class="control-label">Sexo</label>
                    <div class="controls">
                      <label class="radio">
                      <input type="radio" name="sexo_persona_rep" id="sexo_persona_rep" value="Masculino" <?php
                      if ($representante[0]->sexo_persona == "Masculino") {
                        echo 'checked="checked"';
                      }?>>
                          Masculino
                      </label>
                      <label class="radio">
                        <input type="radio" name="sexo_persona_rep" id="sexo_persona_rep" value="Femenino" <?php
                      if ($representante[0]->sexo_persona == "Femenino") {
                        echo 'checked="checked"';
                      }?>>
                          Femenino
                      </label>
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Nacionalidad</label>
                    <div class="controls docs-input-sizes">
                      <input name="nacionalidad_persona_rep" class="span4" type="text" placeholder="Nacionalidad" value="<?=$representante[0]->nacionalidad_persona?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label">Edad</label>
                    <div class="controls docs-input-sizes">
                      <input name="edad_representante" id="edad_representante" class="span2" type="text" value="<?=$representante[0]->edad_representante?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="select01">Parentesco</label>
                    <div class="controls">
                      <select id="parentesco_representante" name="parentesco_representante">
                        <option value="Padre" <?php if ($representante[0]->parentesco_representante == "Padre") { echo "selected"; }?>>Padre</option>
                        <option value="Madre" <?php if ($representante[0]->parentesco_representante == "Madre") { echo "selected"; }?>>Madre</option>
                        <option value="Abuel@" <?php if ($representante[0]->parentesco_representante == "Abuel@") { echo "selected"; }?>>Abuel@</option>
                        <option value="Tí@" <?php if ($representante[0]->parentesco_representante == "Tí@") { echo "selected"; }?>>Tí@</option>
                        <option value="Herman@" <?php if ($representante[0]->parentesco_representante == "Herman@") { echo "selected"; }?>>Herman@</option>
                        <option value="Prim@" <?php if ($representante[0]->parentesco_representante == "Prim@") { echo "selected"; }?>>Prim@</option>
                        <option value="Otr@" <?php if ($representante[0]->parentesco_representante == "Otr@") { echo "selected"; }?>>Otr@</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12">
                <div class="form-actions">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                  <button type="reset" class="btn">Limpiar</button>
                </div>
              </div>            
            </div>

          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <?php require_once("footer.php");?>
  <script type="text/javascript">
      $(function(){
        // Datepicker
        $('#fn_paciente').datepicker({
          inline: true,
          dateFormat    : 'yy-mm-dd',
          dayNames      : ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
          dayNamesMin   : ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
          dayNameShort  : ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
          monthNames    : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
          changeMonth   : true,
          changeYear    : true
        });


        <?php 

          if (!empty($adulto['es_adulto'])) {
            echo '$(".adulto").show("slow"); $(".nino").hide("slow");';
          } else {
            echo '$(".adulto").hide("slow"); $(".nino").show("slow");'; 
          }

        ?>

        

        $("#is_adulto").click(function(){
          if ($(this).is(':checked')) { 
            $(".adulto").show('slow');
            $(".nino").hide('slow');
          } else {
            $(".adulto").hide('slow');
            $(".nino").show('slow');
          }
        });
      });
  </script>
</body>
</html>
