-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 12, 2012 at 12:11 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `consultas`
--

-- --------------------------------------------------------

--
-- Table structure for table `especialidades`
--

CREATE TABLE IF NOT EXISTS `especialidades` (
  `id_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_especialidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_especialidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `especialidades`
--

INSERT INTO `especialidades` (`id_especialidad`, `nombre_especialidad`) VALUES
(1, 'Neurología'),
(2, 'Fisiología'),
(3, 'Traumatología'),
(4, 'Pediatría X');

-- --------------------------------------------------------

--
-- Table structure for table `historias`
--

CREATE TABLE IF NOT EXISTS `historias` (
  `id_historia` int(11) NOT NULL AUTO_INCREMENT,
  `num_historia` varchar(45) DEFAULT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `tipo_historia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_historia`),
  KEY `historia_paciente` (`paciente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `medicos`
--

CREATE TABLE IF NOT EXISTS `medicos` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) DEFAULT NULL,
  `especialidad_id` int(11) DEFAULT NULL,
  `primeras_medico` int(11) DEFAULT NULL,
  `controles_medico` int(11) DEFAULT NULL,
  `reposos_medico` int(11) DEFAULT NULL,
  `cedula_medico` varchar(15) DEFAULT NULL,
  `MPPSPS_medico` varchar(45) DEFAULT NULL,
  `condicion_medico` enum('Fijo','Contratado','Suplente') DEFAULT NULL,
  PRIMARY KEY (`id_medico`),
  KEY `medico_especialidad` (`especialidad_id`),
  KEY `medico_persona` (`persona_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `medicos`
--

INSERT INTO `medicos` (`id_medico`, `persona_id`, `especialidad_id`, `primeras_medico`, `controles_medico`, `reposos_medico`, `cedula_medico`, `MPPSPS_medico`, `condicion_medico`) VALUES
(14, 11, 3, 1, 2, 5, 'V-14460455', 'RYTRE765765', 'Fijo'),
(19, 16, 1, 5, 5, 5, 'V-5109690', 'HRGWJEHREW', 'Contratado'),
(23, 20, 2, 5, 2, 5, 'E-5050235', 'EEEEEEEEEE', 'Suplente');

-- --------------------------------------------------------

--
-- Table structure for table `pacadulto`
--

CREATE TABLE IF NOT EXISTS `pacadulto` (
  `id_pacadulto` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `cedula_paciente` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_pacadulto`),
  KEY `adulto_paciente` (`paciente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pacientes`
--

CREATE TABLE IF NOT EXISTS `pacientes` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) DEFAULT NULL,
  `fn_paciente` date DEFAULT NULL,
  `asegurado_paciente` enum('Si','No') DEFAULT 'No',
  `beneficiario_paciente` enum('Si','No') DEFAULT 'No',
  PRIMARY KEY (`id_paciente`),
  KEY `paciente_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pacnino`
--

CREATE TABLE IF NOT EXISTS `pacnino` (
  `id_pacnino` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) DEFAULT NULL,
  `representante_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pacnino`),
  KEY `nino_paciente` (`paciente_id`),
  KEY `nino_representante` (`representante_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_persona` varchar(45) DEFAULT NULL,
  `apellidos_persona` varchar(45) DEFAULT NULL,
  `direccion_persona` text,
  `sexo_persona` enum('Masculino','Femenino') DEFAULT NULL,
  `nacionalidad_persona` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_persona`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id_persona`, `nombres_persona`, `apellidos_persona`, `direccion_persona`, `sexo_persona`, `nacionalidad_persona`) VALUES
(11, 'Johnmer', 'Bencomo', ' Valera', 'Masculino', 'Venezolana'),
(16, 'Magalis', 'Calderas P', 'Valera', 'Femenino', 'Venezolana'),
(20, 'Carlos', 'Gaviria', ' Barinas', 'Masculino', 'Colombiana');

-- --------------------------------------------------------

--
-- Table structure for table `representantes`
--

CREATE TABLE IF NOT EXISTS `representantes` (
  `id_representante` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) DEFAULT NULL,
  `edad_representante` int(11) DEFAULT NULL,
  `cedula_representante` varchar(15) DEFAULT NULL,
  `parentesco_representante` enum('Padre','Madre','Abuel@','Tí@','Herman@','Prim@','Otr@') DEFAULT 'Padre',
  PRIMARY KEY (`id_representante`),
  KEY `representante_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `nivel` enum('Administrador','Supervisor','Limitado') DEFAULT 'Administrador',
  `usuario` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`, `apellido`, `cargo`, `nivel`, `usuario`, `clave`) VALUES
(1, 'Administrador', 'Administrador', 'Administrador', 'Administrador', 'admin', 'e10adc3949ba59abbe56e057f20f883e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
